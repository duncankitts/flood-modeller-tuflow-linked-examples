# Flood Modeller-TUFLOW Linked Examples

This project provides a number of linked Flood Modeller-TUFLOW linked models to provide examples of the Flood Modeller-TUFLOW Linking Mechanisms which may be of use to users.

The FM_TUFLOW_SX_Connection_Example is a small model which show's how a SX connection can be used to connect between FM 1D domain and TUFLOW's 2D domaon and vice versa.  The SX connection is connected to a dummy HT boundary within the FM 1D domain at both the upstream and downstream extent of the model.  For stability purposes, the upstream HT boundary is connected via a spill unit to the 1D channel.

The FM_TUFLOW_HX_Connection_Example is a similar small model which show's how a HX connection can be used to connect between FM and TUFLOW and vice versa.  In this case the HX connection can be made directly to river units.

The FM_TFULOW_Estry_X1DQ_X1DH_Link_Example is a more complex model based on the TUFLOW Tutorial model which provides an example of HX linking along riverbanks connecting the 1D domain to the 2D TUFLOW domain.  The model also has a pipe network represented within TUFLOW's 1D engine Estry which is connected to Flood Modeller using an X1DH link.  The Flood Modeller model is then connected to a downstrean Estry open channel reach using an X1DQ link.  Both Estry to Flood Modeller links are described in the following page: https://wiki.tuflow.com/index.php?title=TUFLOW_1D_Flood_Modeller_Connectivity

